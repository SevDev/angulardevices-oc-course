# AngularDevices (a.k.a. MyAngProj)
A website made with Angular while following OpenClassrooms' course. Some things may differ from the original course because of Angular and Bootstrap updates + personnal choices.

## Links
* [OpenClassrooms' course](https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular) ;

# Requirements
* NodeJS & npm ;
* Angular CLI (`npm i -g @angular/cli`) ;

## Installation
* Clone or download the files of this repository ;
* Hop in the download folder and type `npm i` to download the needed node modules ;
* Once downloaded, you can type `ng serve` to start the dev server ;
* Open your browser and go to `http://localhost:4200` and enjoy (or suffer, depends) !
