import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'

import { DeviceComponent } from './device/device.component'
import { DeviceService } from './services/device.service'

import { AuthComponent } from './auth/auth.component'
import { AuthService } from './services/auth.service'

import { DeviceViewComponent } from './device-view/device-view.component'
import { SingleDeviceComponent } from './single-device/single-device.component'
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component'

import { Routes, RouterModule } from '@angular/router'
import { AuthGuard } from './services/auth-guard.service'
import { EditDeviceComponent } from './edit-device/edit-device.component'

const appRoutes: Routes = [
	{
		path: 'devices',
		component: DeviceViewComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'devices/:id',
		component: SingleDeviceComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'edit',
		component: EditDeviceComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'auth',
		component: AuthComponent
	},
	{
		path: '',
		canActivate: [AuthGuard],
		redirectTo: '/devices',
		pathMatch: 'full'
	},
	{
		path: 'not-found',
		component: FourOhFourComponent
	},
	{
		path: '**',
		redirectTo: '/not-found'
	}
]

@NgModule({
	declarations: [
		AppComponent,
		DeviceComponent,
		AuthComponent,
		DeviceViewComponent,
		SingleDeviceComponent,
		FourOhFourComponent,
		EditDeviceComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		RouterModule.forRoot(appRoutes)
	],
	providers: [
		DeviceService,
		AuthService,
		AuthGuard
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
