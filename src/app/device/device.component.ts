import { Component, OnInit, Input } from '@angular/core'
import { I18nPluralPipe } from '@angular/common'
import { DeviceService } from '../services/device.service'

@Component({
	selector: 'app-device',
	templateUrl: './device.component.html',
	styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {

	@Input() deviceName: string
	@Input() deviceStatus: string
	@Input() deviceId: number
	@Input() idxDevice: number

	constructor(private deviceService: DeviceService) { }

	ngOnInit() {
	}

	getStatus() {
		return this.deviceStatus
	}

	getColor() {
		if (this.deviceStatus === 'on') {
			return 'green';
		} else if (this.deviceStatus === 'off') {
			return 'red';
		} else if (this.deviceStatus === 'sleep') {
			return 'orange';
		}
	}

	onSwitchOn() {
		this.deviceService.switchOn(this.idxDevice)
	}

	onSwitchOff() {
		this.deviceService.switchOff(this.idxDevice)
	}

	onSwitchSleep() {
		this.deviceService.switchSleep(this.idxDevice)
	}
}
