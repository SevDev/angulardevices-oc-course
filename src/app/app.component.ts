import { Component, OnInit, OnDestroy } from '@angular/core'
import { Observable } from 'rxjs'
import { interval } from 'rxjs'
import { Subscription } from 'rxjs'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

	seconds: number = 0
	counterSubscription: Subscription

	constructor() { }

	ngOnInit() {
		const counter = interval(1000)
		this.counterSubscription = counter.subscribe((value: number) => {
			this.seconds = value
		})
	}

	ngOnDestroy() {
		this.counterSubscription.unsubscribe()
	}
}
