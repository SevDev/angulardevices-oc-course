import { Subject } from 'rxjs'

export class DeviceService {

	deviceSubject = new Subject<any[]>()

	private devices = [
		{
			id: 1,
			name: "TV",
			status: "on"
		},
		{
			id: 2,
			name: "Toaster",
			status: "sleep"
		},
		{
			id: 3,
			name: "Laptop",
			status: "off"
		}
	]

	emitDeviceSubject() {
		this.deviceSubject.next(this.devices.slice())
	}

	getDeviceById(id: number) {
		const device = this.devices.find((deviceObject) => {
			return deviceObject.id === id
		})
		return device
	}

	addDevice(name: string, status: string) {
		const deviceObject = {
			id: 0,
			name: "",
			status: ""
		}
		
		deviceObject.name = name
		deviceObject.status = status
		deviceObject.id = this.devices[(this.devices.length - 1)].id + 1

		this.devices.push(deviceObject)
		this.emitDeviceSubject
	}

	switchOnAll() {
		for (let device of this.devices) {
			device.status = "on"
		}
		this.emitDeviceSubject()
	}

	switchOffAll() {
		for (let device of this.devices) {
			device.status = "off"
		}
		this.emitDeviceSubject()
	}

	switchOn(index: number) {
		this.devices[index].status = "on"
		this.emitDeviceSubject()
	}

	switchOff(index: number) {
		this.devices[index].status = "off"
		this.emitDeviceSubject()
	}

	switchSleep(index: number) {
		this.devices[index].status = "sleep"
		this.emitDeviceSubject()
	}
}